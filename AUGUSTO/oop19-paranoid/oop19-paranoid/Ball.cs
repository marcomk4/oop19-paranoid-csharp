﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop19_paranoid
{
    public class Ball : GameObj
    {

        private Ball(P2d pos, V2d dir, int height, int width, double speed) : 
            base(pos, dir, speed, height, width, new BallPhisicsComponent(), new DummyInputComponent())
        { }

        public void flipVelOnY()
        {
            this.Vel = new V2d(this.Vel.X, -this.Vel.Y);
        }

        public void flipVelOnX()
        {
            this.Vel = new V2d(-this.Vel.X, this.Vel.Y);
        }

        public override void UpdateInput(IInputController controller)
        {
            base.Input.Update(this, controller);
        }

        public override void UpdatePhysics(int dt)
        {
            base.Phys.Update(dt, this);
        }

        public class Builder
        {
            private P2d pos;
            private V2d dir;
            private int height;
            private int width;
            private double speed;

            public Builder Position(P2d pos)
            {
                this.pos = pos;
                return this;
            }
            public Builder Direction(V2d dir)
            {
                this.dir = dir;
                return this;
            }
            public Builder Height(int height)
            {
                this.height = height;
                return this;
            }
            public Builder Width(int width)
            {
                this.width = width;
                return this;
            }
            
            public Builder Speed(double speed)
            {
                this.speed = speed;
                return this;
            }
            
            public Ball Build()
            {
                if (this.pos == null || this.dir == null  || this.height <= 0 || this.width <= 0)
                {
                    throw new InvalidOperationException();
                }
                return new Ball(this.pos, this.dir, this.height, this.width, this.speed);
            }
        }
    }
}
