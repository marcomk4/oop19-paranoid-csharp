﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop19_paranoid
{
    [Serializable]
    public class BallPhisicsComponent : IPhysicsComponent
    {
        public void Update(int dt, IGameObject gameObj)
        {
            Ball ball = (Ball)gameObj;
            P2d old = ball.Pos;
            V2d vel = ball.Vel;
            ball.Pos = ball.Pos+(vel*(0.001 * dt * ball.Speed));
        }
    }
}
