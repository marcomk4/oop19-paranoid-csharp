﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop19_paranoid
{

    public class Border
    {
        public P2d upperLefCorner { get; private set; }
        public P2d bottomRightCorner { get; private set; }
        public double width { get; private set; }
        public double height  { get; private set; }

        public Border(double width, double height)
        {
            this.width = width;
            this.height = height;
            this.upperLefCorner = new P2d(0, 0);
            this.bottomRightCorner = new P2d(width, height);
            
        }
    }

}
