﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop19_paranoid
{
    [Serializable]
    public class DummyInputComponent : IInputComponent
    {
        public void Update(IGameObject obj, IInputController c)
        {
        }
    }
}
