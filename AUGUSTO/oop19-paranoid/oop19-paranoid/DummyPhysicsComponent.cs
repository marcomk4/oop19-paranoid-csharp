﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop19_paranoid
{
    [Serializable]
    public class DummyPhysicsComponent : IPhysicsComponent
    {
        public void Update(int dt, IGameObject gameObj)
        {
        }
    }
}
