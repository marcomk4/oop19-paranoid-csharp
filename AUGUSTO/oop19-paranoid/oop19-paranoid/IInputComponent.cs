﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop19_paranoid
{
    public interface IInputComponent
    {
        void Update(IGameObject obj, IInputController c);
    }
}
