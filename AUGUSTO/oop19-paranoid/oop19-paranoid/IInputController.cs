﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop19_paranoid
{
    public interface IInputController
    {
        bool IsMoveRight();
        bool IsMoveLeft();
        void NotifyMoveRight(bool condition);
        void NotifyMoveLeft(bool condition);
    }
}
