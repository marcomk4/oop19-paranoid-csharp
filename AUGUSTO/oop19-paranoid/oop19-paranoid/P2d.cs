﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop19_paranoid
{
    [Serializable]
    public class P2d
    {
        public double X { get; private set; }
        public double Y { get; private set; }

        public P2d (double x, double y)
        {
            X = x;
            Y = y;
        }

        public static P2d operator +(P2d p1, V2d p2)
        {
            return new P2d(p1.X + p2.X, p1.Y + p2.Y);
        }

        public static P2d operator -(P2d p1, P2d p2)
        {
            return new P2d(p1.X - p2.X, p1.Y - p2.Y);
        }
        
        public override string ToString()
        {
            return $"P2d( {X} , {Y} )";
        }

        public override bool Equals(object obj)
        {
            return obj is P2d d &&
                   X == d.X &&
                   Y == d.Y;
        }

        public override int GetHashCode()
        {
            var hashCode = 1861411795;
            hashCode = hashCode * -1521134295 + X.GetHashCode();
            hashCode = hashCode * -1521134295 + Y.GetHashCode();
            return hashCode;
        }
    }
}
