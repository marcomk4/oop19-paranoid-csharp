﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop19_paranoid
{
    [Serializable]
    public class V2d
    {
        public V2d(double x, double y)
        {
            X = x;
            Y = y;
        }
        public double X { get; private set; }
        public double Y { get; private set; }

        public static V2d operator +(V2d p1, V2d p2)
        {
            return new V2d(p1.X + p2.X, p1.Y + p2.Y);
        }
        
        public double Module()
        {
            return (double)Math.Sqrt(X * X + Y * Y);
        }

        public V2d GetNormalized()
        {
            double module = (double)Math.Sqrt(X * X + Y * Y);
            return new V2d(X / module, Y / module);
        }

        public static V2d operator *(V2d p1, double fact)
        {
            return new V2d(p1.X * fact, p1.Y * fact);
        }

        public override string ToString()
        {
            return $"V2d( {X} , {Y} )";
        }

        public override bool Equals(object obj)
        {
            return obj is V2d d &&
                   X == d.X &&
                   Y == d.Y;
        }

        public override int GetHashCode()
        {
            var hashCode = 1861411795;
            hashCode = hashCode * -1521134295 + X.GetHashCode();
            hashCode = hashCode * -1521134295 + Y.GetHashCode();
            return hashCode;
        }
    }
}
