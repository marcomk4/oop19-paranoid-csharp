﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop19_paranoid
{

    public class World
    {
        private HashSet<Ball> balls;
        private Border border;

        public World(Border border)
        {
            this.border = border;
            this.balls = new HashSet<Ball>();
        }

        public void SetBalls(HashSet<Ball> balls)
        {
            this.balls.Clear();
            foreach (Ball ball in balls)
            {
                this.balls.Add(ball);
            }
        }

        public HashSet<IGameObject> GetSceneEntity()
        {
            HashSet<IGameObject> ent = new HashSet<IGameObject>();
            //repeat this for all kind of entity
            foreach (Ball ball in balls)
            {
                ent.Add(ball);
            }
            return ent;
        }

        public void UpdateState(int dt)
        {
            foreach (GameObj gobj in this.GetSceneEntity())
            {
                gobj.UpdatePhysics(dt);
            }
        }
    }
}
