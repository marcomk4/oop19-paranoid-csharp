﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using oop19_paranoid;
using System.Collections.Generic;

namespace test
{
    [TestClass]
    public class BallTest
    {
        [TestMethod]
        public void FailBallCreation()
        {
            Ball.Builder ballBuilder = new Ball.Builder().Position(null);
            Assert.ThrowsException<InvalidOperationException>(() => ballBuilder.Build());
            ballBuilder.Position(new P2d(10, 10))
                       .Direction(null);
            Assert.ThrowsException<InvalidOperationException>(() => ballBuilder.Build());
            ballBuilder.Position(new P2d(10, 10))
                       .Direction(new V2d(1, 0))
                       .Height(-1);
            Assert.ThrowsException<InvalidOperationException>(() => ballBuilder.Build());
            ballBuilder.Position(new P2d(10, 10))
                       .Direction(new V2d(1, 0))
                       .Height(10)
                       .Width(-1);
            Assert.ThrowsException<InvalidOperationException>(() => ballBuilder.Build());
        }

        [TestMethod]
        public void BallCreation()
        {
            Ball ball = new Ball.Builder().Position(new P2d(10, 10))
                                          .Direction(new V2d(0, 1))
                                          .Height(10)
                                          .Width(10)
                                          .Speed(10)
                                          .Build();
            Assert.AreEqual(new P2d(10, 10), ball.Pos);
            Assert.AreEqual(new V2d(0, 1), ball.Vel);
            Assert.AreEqual(10, ball.Speed);
            Assert.AreEqual(10, ball.Height);
            Assert.AreEqual(10, ball.Width);
        }

        [TestMethod]
        public void BallMovement()
        {
            World world = new World(new Border(100, 100));
            Ball.Builder ballBuilder = new Ball.Builder();
            ballBuilder.Height(10)
                       .Width(10)
                       .Speed(200);
            
            // north direction
            double py = Math.Sin((Math.PI / 180) * 90);
            double px = Math.Cos((Math.PI / 180) * 90);
            ballBuilder.Position(new P2d(50, 50))
                       .Direction(new V2d(px, py));
            HashSet<Ball> ballContainer = new HashSet<Ball>();
            ballContainer.Add(ballBuilder.Build());
            world.SetBalls(ballContainer);
            foreach (Ball ent in world.GetSceneEntity())
            {
                Assert.AreEqual(new P2d(50, 50), ent.Pos);
            }
            world.UpdateState(10);
            foreach (Ball ent in world.GetSceneEntity())
            {
                Assert.AreEqual(new P2d(50, 52), ent.Pos);
            }

            // south direction
            py = Math.Sin((Math.PI / 180) * 270);
            px = Math.Cos((Math.PI / 180) * 270);
            ballBuilder.Position(new P2d(50, 50))
                       .Direction(new V2d(px, py));
            ballContainer = new HashSet<Ball>();
            ballContainer.Add(ballBuilder.Build());
            world.SetBalls(ballContainer);
            foreach (Ball ent in world.GetSceneEntity())
            {
                Assert.AreEqual(new P2d(50, 50), ent.Pos);
            }
            world.UpdateState(10);
            foreach (Ball ent in world.GetSceneEntity())
            {
                Assert.AreEqual(new P2d(50, 48), ent.Pos);
            }
            
            // east direction
            py = Math.Sin((Math.PI / 180) * 0);
            px = Math.Cos((Math.PI / 180) * 0);
            ballBuilder.Position(new P2d(50, 50))
                       .Direction(new V2d(px, py));
            ballContainer = new HashSet<Ball>();
            ballContainer.Add(ballBuilder.Build());
            world.SetBalls(ballContainer);
            foreach (Ball ent in world.GetSceneEntity())
            {
                Assert.AreEqual(new P2d(50, 50), ent.Pos);
            }
            world.UpdateState(10);
            foreach (Ball ent in world.GetSceneEntity())
            {
                Assert.AreEqual(new P2d(52, 50), ent.Pos);
            }

            // west direction
            py = Math.Sin((Math.PI / 180) * 180);
            px = Math.Cos((Math.PI / 180) * 180);
            ballBuilder.Position(new P2d(50, 50))
                       .Direction(new V2d(px, py));
            ballContainer = new HashSet<Ball>();
            ballContainer.Add(ballBuilder.Build());
            world.SetBalls(ballContainer);
            foreach (Ball ent in world.GetSceneEntity())
            {
                Assert.AreEqual(new P2d(50, 50), ent.Pos);
            }
            world.UpdateState(10);
            foreach (Ball ent in world.GetSceneEntity())
            {
                Assert.AreEqual(new P2d(48, 50), ent.Pos);
            }
        }

        [TestMethod]
        public void BallSpeed()
        {
            World world = new World(new Border(100, 100));
            Ball.Builder ballBuilder = new Ball.Builder();
            ballBuilder.Height(10)
                       .Width(10);
            double py = Math.Sin((Math.PI / 180) * 0);
            double px = Math.Cos((Math.PI / 180) * 0);
            ballBuilder.Position(new P2d(50, 50))
                       .Direction(new V2d(px, py));
            HashSet<Ball> ballContainer = new HashSet<Ball>();
            ballContainer.Add(ballBuilder.Build());
            world.SetBalls(ballContainer);

            // 0 speed
            ballBuilder.Speed(0);
            world.SetBalls(ballContainer);
            foreach (Ball ent in world.GetSceneEntity())
            {
                Assert.AreEqual(new P2d(50, 50), ent.Pos);
            }
            world.UpdateState(1000000);
            foreach (Ball ent in world.GetSceneEntity())
            {
                Assert.AreEqual(new P2d(50, 50), ent.Pos);
            }

            // 100 speed
            ballContainer = new HashSet<Ball>();
            ballBuilder.Speed(100);
            ballContainer.Add(ballBuilder.Build());
            world.SetBalls(ballContainer);
            foreach (Ball ent in world.GetSceneEntity())
            {
                Assert.AreEqual(new P2d(50, 50), ent.Pos);
            }
            world.UpdateState(10);
            foreach (Ball ent in world.GetSceneEntity())
            {
                Assert.AreEqual(new P2d(51, 50), ent.Pos);
            }

            // 1000 speed
            ballContainer = new HashSet<Ball>();
            ballBuilder.Speed(1000);
            ballContainer.Add(ballBuilder.Build());
            world.SetBalls(ballContainer);
            foreach (Ball ent in world.GetSceneEntity())
            {
                Assert.AreEqual(new P2d(50, 50), ent.Pos);
            }
            world.UpdateState(10);
            foreach (Ball ent in world.GetSceneEntity())
            {
                Assert.AreEqual(new P2d(60, 50), ent.Pos);
            }
        }

    }
}
