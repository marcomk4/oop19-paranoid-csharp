﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using oop19_paranoid;
using System.Collections.Generic;

namespace test
{
    [TestClass]
    public class WorldTest
    {
        [TestMethod]
        public void FailBallCreation()
        {
            World world = new World(new Border(600, 600));
            Ball.Builder ballBuilder = new Ball.Builder();
            ballBuilder.Position(new P2d(10, 10))
                                          .Direction(new V2d(0, 1))
                                          .Height(10)
                                          .Width(10)
                                          .Speed(10);
            Assert.IsTrue(world.GetSceneEntity().Count == 0);
            HashSet<Ball> ballsContainer = new HashSet<Ball>();
            for(int i = 0; i < 100; i++)
            {
                ballsContainer.Add(ballBuilder.Build());
            }
            world.SetBalls(ballsContainer);
            Assert.AreEqual(100, world.GetSceneEntity().Count);
        }
    }
}
