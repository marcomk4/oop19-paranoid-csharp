﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using oop19_paranoid_csharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyTest
{
    [TestClass]
    public class TestBrick
    {
        private Brick brick;

        [TestInitialize]
        public void CreateEntity()
        {
            this.brick = new Brick.Builder().Position(new P2d(50, 50))
                                            .Height(10)
                                            .Width(20)
                                            .PointEarned(50)
                                            .Energy(1)
                                            .Indestructible(false)
                                            .Build();
        }

        [TestMethod]
        public void TestBrickCreation()
        {
            Assert.AreEqual(new P2d(50, 50), this.brick.Pos);
            Assert.AreEqual(20, this.brick.Width);
            Assert.AreEqual(10, this.brick.Height);
            Assert.ThrowsException<InvalidOperationException>(() => new Brick.Builder().Build());
            Assert.ThrowsException<InvalidOperationException>(() => new Brick.Builder().Position(null).Build());
            Assert.ThrowsException<InvalidOperationException>(() => new Brick.Builder().Position(new P2d(100, 100))
                                                                                        .Width(20)
                                                                                        .Height(10)
                                                                                        .Energy(-1)
                                                                                        .Build());

        }
    }
}
