﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using oop19_paranoid_csharp;

namespace MyTest
{
    [TestClass]
    public class TestPlayer
    {
        private Player e1;
        private Player e2;
        private readonly Dictionary<Player.PlayerId, IInputController> inputController =
            new Dictionary<Player.PlayerId, IInputController>();
        private readonly HashSet<Player> players = new HashSet<Player>();

        [TestInitialize]
        public void CreateEntity()
        {
            this.e1 = CreatePlayer(new P2d(290, 580), Player.PlayerId.ONE);
            this.e2 = CreatePlayer(new P2d(190, 580), Player.PlayerId.TWO);
            this.inputController.Add(Player.PlayerId.ONE, new KeyboardInputController());
            this.inputController.Add(Player.PlayerId.TWO, new KeyboardInputController());
            this.players.Add(e1);
            this.players.Add(e2);
        }

        [TestMethod]
        public void TestPlayerCreation()
        {
            Assert.AreEqual(new P2d(290, 580), this.e1.Pos);
            Assert.AreEqual(new P2d(190, 580), this.e2.Pos);
            Assert.AreEqual(78, this.e1.Width);
            Assert.AreEqual(20, this.e1.Height);
            Assert.AreEqual(Player.PlayerId.ONE, this.e1.Pid);
            Assert.AreEqual(Player.PlayerId.TWO, this.e2.Pid);
            Assert.ThrowsException<InvalidOperationException>(() => new Player.Builder().Build());
            Assert.ThrowsException<InvalidOperationException>(() => new Player.Builder().Position(null).Build());
            Assert.ThrowsException<InvalidOperationException>(() => new Player.Builder().Position(new P2d(100, 100))
                                                                                        .Width(-1)
                                                                                        .Height(10)
                                                                                        .Build());

        }

        [TestMethod]
        public void TestPlayerMovement()
        {
            Assert.AreEqual(new P2d(290, 580), e1.Pos);
            Assert.AreEqual(new P2d(190, 580), e2.Pos);
            inputController.TryGetValue(Player.PlayerId.ONE, out IInputController controller);
            controller.NotifyMoveRight(true);
            inputController.TryGetValue(Player.PlayerId.TWO, out controller);
            controller.NotifyMoveLeft(true);
            foreach (KeyValuePair<Player.PlayerId, IInputController> kv in inputController)
            {
                MovePlayer(kv.Key, kv.Value);
            }
            UpdateState(20);
            Assert.AreEqual(new P2d(299, 580), e1.Pos);
            Assert.AreEqual(new P2d(181, 580), e2.Pos);
        }

        private Player CreatePlayer(P2d pos, Player.PlayerId playerId)
        {
            return new Player.Builder()
                             .Position(pos)
                             .Width(78)
                             .Height(20)
                             .PlayerId(playerId)
                             .Build();
        }
        private void MovePlayer(Player.PlayerId playerId, IInputController controller)
        {
            foreach(Player p in players)
            {
                if (p.Pid == playerId)
                {
                    p.UpdateInput(controller);
                }       
            }
        }
        private void UpdateState(int dt)
        {
            foreach(Player p in players)
            {
                p.UpdatePhysics(dt);
            }
        }
        public static void Main(string[] args)
        {
            Console.Write(args);
        }
    }
}
