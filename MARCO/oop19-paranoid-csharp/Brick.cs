﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop19_paranoid_csharp
{
    public class Brick : GameObj
    {
        private readonly bool indestructible;

        private Brick(P2d pos, int height, int width, int pointEarned, int energy, bool indestructible) : base(
            pos, new V2d(0, 0), 0, height, width, new DummyPhysicsComponent(), new DummyInputComponent())
        {
            PointEarned = pointEarned;
            Energy = energy;
            this.indestructible = indestructible;
        }
        public int PointEarned {get; private set; }
        public int Energy { get; private set; }

        public void DecEnergy()
        {
            Energy--;
        }

        public Boolean IsIndestructible()
        {
            return this.indestructible;
        }

        public override void UpdateInput(IInputController controller)
        {
            base.Input.Update(this, controller);
        }

        public override void UpdatePhysics(int dt)
        {
            base.Phys.Update(dt, this);
        }

        public class Builder
        {
            private P2d pos;
            private int height;
            private int width;
            private int pointEarned;
            private int energy;
            private bool indestructible;

            public Builder Position (P2d pos)
            {
                this.pos = pos;
                return this;
            }
            public Builder Height(int height)
            {
                this.height = height;
                return this;
            }
            public Builder Width(int width)
            {
                this.width = width;
                return this;
            }
            public Builder PointEarned(int pointEarned)
            {
                this.pointEarned = pointEarned;
                return this;
            }
            public Builder Energy(int energy)
            {
                this.energy = energy;
                return this;
            }
            public Builder Indestructible(bool indestructible)
            {
                this.indestructible = indestructible;
                return this;
            }
            public Brick Build()
            {
                if (this.pos == null || this.height <= 0 || this.width <= 0
                        || this.energy <= 0)
                {
                    throw new InvalidOperationException();
                }
                return new Brick(this.pos, this.height, this.width, this.pointEarned, this.energy, this.indestructible);
            }
        }
    }
}
