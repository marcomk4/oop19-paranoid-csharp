﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop19_paranoid_csharp
{
    [Serializable]
    public abstract class GameObj : IGameObject
    {
        public GameObj(P2d pos, V2d vel, double speed, int height, int width, IPhysicsComponent phys, IInputComponent input)
        {
            Pos = pos;
            Vel = vel;
            Speed = speed;
            Height = height;
            Width = width;
            Input = input;
            Phys = phys;
        }
        public P2d Pos { get; set; }
        public V2d Vel { get; set; }
        public double Speed { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public IInputComponent Input { get; private set; }
        public IPhysicsComponent Phys { get; private set; }

        public abstract void UpdateInput(IInputController controller);

        public abstract void UpdatePhysics(int dt);
    }
}
