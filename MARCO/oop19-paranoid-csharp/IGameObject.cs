﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop19_paranoid_csharp
{
    public interface IGameObject
    {
        P2d Pos { get; set; }
        V2d Vel { get; set; }
        double Speed { get; set; }
        int Height { get; set; }
        int Width { get; set; }

        void UpdatePhysics(int dt);
        void UpdateInput(IInputController controller);
    }
}
