﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop19_paranoid_csharp
{
    public interface IPhysicsComponent
    {
        void Update(int dt, IGameObject gameObj);
    }
}
