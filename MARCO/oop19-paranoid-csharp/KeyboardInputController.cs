﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop19_paranoid_csharp
{
    public class KeyboardInputController : IInputController
    {
        private bool moveRight;
        private bool moveLeft;

        public bool IsMoveLeft()
        {
            return this.moveLeft;
        }

        public bool IsMoveRight()
        {
            return this.moveRight;
        }

        public void NotifyMoveLeft(bool condition)
        {
            this.moveLeft = condition;
        }

        public void NotifyMoveRight(bool condition)
        {
            this.moveRight = condition;
        }
    }
}
