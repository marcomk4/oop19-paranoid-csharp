﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop19_paranoid_csharp
{
    public class Player : GameObj
    {
        private const int PLAYER_SPEED = 450;
        public enum PlayerId { ONE, TWO};
        private Player (P2d pos, int height, int width, PlayerId playerId) : base (
            pos, new V2d(0, 0), PLAYER_SPEED, height, width, new PlayerPhysicsComponent(), new PlayerInputComponent())
        {
            Pid = playerId;
        }
        public PlayerId Pid { get; private set; }
        public override void UpdateInput(IInputController controller)
        {
            base.Input.Update(this, controller);
        }

        public override void UpdatePhysics(int dt)
        {
            base.Phys.Update(dt, this);
        }

        public class Builder
        {
            private P2d pos;
            private int height;
            private int width;
            private PlayerId playerId;

            public Builder Position(P2d pos)
            {
                this.pos = pos;
                return this;
            }
            public Builder Height(int height)
            {
                this.height = height;
                return this;
            }
            public Builder Width(int width)
            {
                this.width = width;
                return this;
            }
            public Builder PlayerId(PlayerId playerId)
            {
                this.playerId = playerId;
                return this;
            }
            public Player Build()
            {
                if (this.pos == null || this.height <= 0 || this.width <= 0)
                {

                    throw new InvalidOperationException();
                }
                return new Player(this.pos, this.height, this.width, this.playerId);
            }
        }
    }
}
