﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop19_paranoid_csharp
{
    public class PlayerInputComponent : IInputComponent
    {
        public void Update(IGameObject obj, IInputController c)
        {
            Player player = (Player)obj;
            if (c.IsMoveRight())
            {
                player.Vel = new V2d(1,0);
            }
            else if (c.IsMoveLeft())
            {
                player.Vel = new V2d(-1, 0);
            }
            else
            {
                player.Vel = new V2d(0, 0);
            }
        }
    }
}
