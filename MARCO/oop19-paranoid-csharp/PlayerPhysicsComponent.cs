﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop19_paranoid_csharp
{
    public class PlayerPhysicsComponent : IPhysicsComponent
    {
        private const double SCALER = 0.001;
        public void Update(int dt, IGameObject gameObj)
        {
            Player player = (Player)gameObj;
            P2d posPlayer = player.Pos;
            V2d velPlayer = player.Vel;
            player.Pos = posPlayer+(velPlayer*(SCALER * dt * player.Speed));
            
        }
    }
}
